# Aceso generic TV app for LG Pro:Centric devices

## Summary

This app provides FTG (Free-To-Guest) TV functionality on LG Pro:Centric devices including 
Pro:Centric TVs and set-top boxes.

## Configuration

`config.prod.json` file is the configuration file.

| config item | type | optional | description | default value |
|--------|------|------|-------------|---------|
|`offlineMode`|boolean| |Indicating if the app is working in offline mode (local mode).  When the app is running in offline mode it will be pre-loaded on the device flash memory and device will always boot up with this app.  The app provide TV functionality but also checks server availability.  It will attempt to redirect device to server portal when it's available.|`false`|
|`server`|string| |The base URL of the server to redirect to.||
|`redirectPath`|string|optional|The redirect path on the server.  This is where online app bootstrap starts from.  In UpServer backend is this always `/startup/`.  In legacy SeaChange this might be `ewf`.|`"/startup/"`|
|`checkPoints`|array|optional|A list of path to check for server availability.  App will attempt to connect to every path in the URL and only perform redirect when all of them are successful.|`["/startup/","/api/v1/time"]`|

## Developer

`npm start` will start a local webpack-dev-server on port 3000 for testing purpose.

You can also provide a `config.json` file to overwrite some configuration items without
touching the config.prod.json file in the repo.  This is usually handy if you want to test
against your local backend.