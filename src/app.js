import $ from 'jquery';
import './helpers/dotimeout';
import TvScreen from './service/tvscreen';
import KeyCodes from './device/keycodes';

import './css/app.css';

if (navigator.userAgent.indexOf(';LGE ;') > -1) {
    console.log("Platform: Procentric -- LOADING!");
    var Procentric = require('./device/procentric').default;
    console.log("Platform: Procentric -- LOADING !!!!");
    window.PelicanDevice = new Procentric();
    console.log("Platform: Procentric");
}
else {
    // If test on PC use default desktop
    var Desktop = require('./device/desktop').default;
    window.PelicanDevice = new Desktop();
    console.log("Platform: Desktop Chrome");
}

// Instantiate App and components
const App = window.App = {
    data: {},

    config: {},

    device: PelicanDevice,

    tv: new TvScreen(),

    cheats: {
        reload: [KeyCodes.Menu, KeyCodes.Num2, KeyCodes.Num5, KeyCodes.Num8, KeyCodes.Num0]
    },

    start: function () {
        var self = this;
        console.log('Starting the APP');

        self.device.bootstrap();
        self.tv.initialize();

        // power handler
        document.addEventListener(
            "power_mode_changed",
            function () {
                console.log("Event 'power_mode_changed' is received");
                self.device.getPower().done(function (isOn) {
                    console.log("Power status: " + isOn);
                    if (isOn) {
                        self.tv.playStartChannel();
                    }
                    else {
                        self.tv.stopTv();
                    }
                });
            },
            false
        );

        // load production config first
        $.getJSON('./config.prod.json')
            .done(function (data) {
                console.log('Configuration loaded successfully from "config.prod.json"');
                self.config = data;
            })
            .fail(function () {
                console.log('Failed to load configuration from "config.prod.json"!');
            })
            .always(function () {
                // always try to load override config
                $.getJSON('./config.json')
                    .done(function (data) {
                        console.log('Override configuration loaded successfully from "config.json"');
                        $.extend(self.config, data);
                    })
                    .always(function () {
                        console.log('Config ready');

                        // load channel ring
                        self.loadChannelMap();

                        // If app is running offline mode we need to check server and try redirect to there
                        if (self.config.offlineMode) {

                            // Delay a random time before connection so we don't crash the server during a massive device reboot
                            var wait = Math.floor(Math.random() * (60000 - 5000) + 5000);
                            console.log('Wait ' + wait + 'ms before attempting to connect to server');
                            $.doTimeout(wait, function () {
                                self.checkServer();

                                // also check server every minute
                                $.doTimeout('check server timer', 60000, function () {
                                    self.checkServer();
                                    return true;
                                });
                            });
                        }
                    });
            });


    },

    loadChannelMap: function () {
        var self = this;

        $.getJSON('./channelmap.json')
            .done(function (data) {
                console.log('Channel map loaded successfully from "channelmap.json"');
                self.tv.channels = data;
                self.playTv();
            })
            .fail(function () {
                console.log('Failed to load channel map from "channelmap.json"!');
            });
    },

    stop: function () {
        this.tv.onScreenHide();
        $.doTimeout('play tv timer');
        $.doTimeout('check server timer');
    },

    checkServer: function () {
        var self = this;
        var server = self.config.server;
        var redirect = server + (self.config.redirectPath || '/startup/');

        console.log('Checking server status:');

        var checks = self.config.checkPoints || ["/startup/","/api/v1/time"];
        if (!$.isArray(checks)) {
            checks = [self.config.checkPoints];
        }

        var dArray = []
        $.each(checks, function (i, v) {
            var d = $.ajax({
                url: server + v,
                cache: false,
                timeout: 5000,
            });

            dArray.push(d);
            console.log('Checking ' + server + v + ' ...');
        });

        $.when(...dArray).done(function () {
            console.log('server is ONLINE!');
            console.log('############ redirect to ' + redirect);
            location.href = redirect;
        }).fail(function () {
            console.log('server is OFFLINE!');
        });
    },

    playTv: function () {
        this.tv.onScreenShow();
        this.tv.playStartChannel();
    },

    triggerKey: function (e, key) {
        e.preventDefault();
        console.log('processing key ' + key);
        this.checkCheatCodes(key);
        this.tv.onKey(e, key);
    },

    checkCheatCodes: function (key) {
        if (this.isCheat(key, this.cheats.reload)) {
            if (PelicanDevice.reload)
                PelicanDevice.reload();
        }
    },

    isCheat: function (key, cheatKeys) {
        var idx = cheatKeys.match || 0;
        var result = false;

        if (!cheatKeys || !cheatKeys.length || !key)
            return false;

        if (idx < 0 || idx >= cheatKeys.length) {
            idx = 0;
            result = false;
        }
        else if (cheatKeys[idx] === key) {
            idx++;   // hit, increase match index

            if (idx >= cheatKeys.length) {    // bingo, all hit
                console.log('^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ');
                console.log('^-^ cheat code detected - "' + cheatKeys.toString() + '" ^-^');
                console.log('^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ');
                idx = 0;
                result = true;
            }
            else {
                result = false;
            }
        }
        else {
            idx = 0; // missed, reset index
            result = false;
        }

        cheatKeys.match = idx;
        return result;
    }
};

window.onunload = function () {
    console.log('ftg app stopping');
    App.stop();
};

window.onload = function () {
    $.doTimeout(1000, function () {
        console.log('ftg app starting');
        App.start();
    });
};

export default App;

