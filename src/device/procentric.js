import jQuery from 'jquery';
import KeyCodes from './keycodes';
var debug = console;

export default class Procentric {
    constructor() {
        debug.log('Procentric Instantiation -- START');
        this.keyHandler = new KeyHandler(this);
        debug.log('Procentric Instantiation -- FINISH');
    }

    /**
     * Start bootstrap process
     */
    bootstrap() {
        document.addEventListener('keydown', this.keyHandler.process.bind(this.keyHandler), false);
        this.setProperty("tv_channel_attribute_floating_ui", "0");
    }

    /**
     * Get power status
     * @return {boolean}
     */
    getPower() {
        var defer = jQuery.Deferred();
        hcap.power.getPowerMode({
            "onSuccess": function (s) {
                console.log("onSuccess power mode " + s.mode);
                defer.resolve(s.mode == hcap.power.PowerMode.NORMAL);
            },
            "onFailure": function (f) {
                console.log("onFailure : errorMessage = " + f.errorMessage);
                defer.reject(f.errorMessage);
            }
        });
        return defer.promise();
    }

    /**
     * Set power status
     */
    setPower(state) {
        var mode = state ? hcap.power.PowerMode.NORMAL : hcap.power.PowerMode.WARM;
        return this.setPowerState(mode);
    }

    setPowerState(param) {
        hcap.power.setPowerMode({
            "mode": param,
            "onSuccess": function (s) {
                debug.log("Power mode has been set to " + param);
            },
            "onFailure": function (f) {
                debug.log("Failed to set power status. errorMessage = " + f.errorMessage);
            }
        });
    }

    /**
     * Play a tv channel
     * @param {Object} param - Parameters object
     * @param {string} param.mode - Tuning Mode, "LogicalNumber", "ChannelNumber", "Frequence", "IPAddressPort"
     * @param {string} param.type - Channel type, "IP", "Analog" or "Digital"
     * @param {string} param.rfType - RF broadcast type, "Cable" or "Air"
     * @param {number} param.logicalNumber
     * @param {number} param.majorNumber - RF channel number
     * @param {number} param.minorNumber - RF minor channel for digital signals
     * @param {string} param.ip - UDP ip address
     * @param {number} param.port - UDP port
     * @return {Object} jQuery promise
     */
    playChannel(param) {
        var self = this;
        var defer = jQuery.Deferred();
        var request = {
            onSuccess: function () {
                defer.resolve();
            },
            onFailure: function (f) {
                defer.reject(f.errorMessage);
            }
        };

        var type = (param.type || 'Analog').toLowerCase();
        var mode = (param.mode || 'ChannelNumber').toLowerCase();


        if (mode == 'logicalnumber') {
            request.logicalNumber = param.logicalNumber;
            request.channelType = hcap.channel.ChannelType.RF;
            request.rfBroadcastType = hcap.channel.RfBroadcastType.CABLE;
        }

        else {

            if (type == 'analog') {
                request.channelType = hcap.channel.ChannelType.RF;
                request.majorNumber = param.majorNumber;
                request.minorNumber = 0;
                request.rfBroadcastType = (param.rfType == 'Air') ? hcap.channel.RfBroadcastType.ANALOG_NTSC : hcap.channel.RfBroadcastType.CABLE;
            }
            else if (type == 'digital') {
                request.channelType = hcap.channel.ChannelType.RF;
                request.majorNumber = param.majorNumber;
                request.minorNumber = param.minorNumber;
                request.rfBroadcastType = (param.rfType == 'Air') ? hcap.channel.RfBroadcastType.TERRESTRIAL : hcap.channel.RfBroadcastType.CABLE;
            }
            else if (type == 'ip') {
                request.channelType = hcap.channel.ChannelType.IP;
                request.ip = param.ip;
                request.port = param.port;
                request.ipBroadcastType = hcap.channel.ipBroadcastType.UDP;
            }
        }

        hcap.channel.requestChangeCurrentChannel(request);
        return defer.promise();
    }


    /**
     * Stop current playing channel
     */
    stopChannel() {
        var self = this;
        var defer = jQuery.Deferred();

        hcap.channel.stopCurrentChannel({
            "onSuccess": function () {
                defer.resolve();
                debug.log("channel successfully stopped");
            },
            "onFailure": function (f) {
                defer.reject(f.errorMessage);
                debug.log("failed to stop channel: " + f.errorMessage);
            }
        });

        return defer.promise();
    }

    /**
     * Set the video layer size
     * @param param object containing values
     * @param param.x - x position of video
     * @param param.y - y position of video
     * @param param.width - width of video
     * @param param.height - height ov video
     */
    setVideoLayerSize(param) {
        hcap.video.setVideoSize({
            "x": param.x,
            "y": param.y,
            "width": param.width,
            "height": param.height,
            "onSuccess": function () {
                debug.log("onSuccess");
            },
            "onFailure": function (f) {
                debug.log("onFailure : errorMessage = " + f.errorMessage);
            }
        });
    }

    /**
     * Get the video layer size
     */
    getVideoLayerSize() {
        debug.log('`getVideoLayerSize` is not implemented');
    }

    /**
     * Display video layer
     */
    showVideoLayer(container) {
        if (!container)
            return;
        var $container = (container instanceof jQuery) ? container : jQuery(container);
        $container.css({background: 'transparent'});
    }

    /**
     * Hide video layer
     */
    hideVideoLayer(container) {
        if (!container)
            return;
        var $container = (container instanceof jQuery) ? container : jQuery(container);
        $container.css({background: ''});
    }

    reload(param) {
        hcap.power.reboot({});
    }

    reboot(param) {
        hcap.power.reboot({});
    }

    setProperty(key, val) {
        hcap.property.setProperty({
            key: key,
            value: val,
            "onSuccess": function () {
                console.log("[setProperty]: Successfully set " + key + " = " + val);
            },
            "onFailure": function (f) {
                console.log("[setProperty]: FAILED to set property " + key + " = " + val + " [ errorMessage = " + f.errorMessage + " ]");
            }
        });
    }
}

class KeyHandler {

    constructor(procentric) {
        this.procentric = procentric;
    }

    process(e) {
        console.log('PROCENTRIC keydown, code ' + e.which);
        var key = keyMap[e.which];
        if (key) {
            App.triggerKey(e, key);
        }
    }
}

const keyMap = {};

keyMap[hcap.key.Code.BACK] = KeyCodes.Back;
keyMap[hcap.key.Code.EXIT] = KeyCodes.Exit;
keyMap[hcap.key.Code.ENTER] = KeyCodes.Enter;
keyMap[hcap.key.Code.PAUSE] = KeyCodes.Pause;
keyMap[hcap.key.Code.PAGE_UP] = KeyCodes.PageUp;
keyMap[hcap.key.Code.PAGE_DOWN] = KeyCodes.PageDown;
keyMap[hcap.key.Code.LEFT] = KeyCodes.Left;
keyMap[hcap.key.Code.UP] = KeyCodes.Up;
keyMap[hcap.key.Code.RIGHT] = KeyCodes.Right;
keyMap[hcap.key.Code.DOWN] = KeyCodes.Down;
keyMap[hcap.key.Code.NUM_0] = KeyCodes.Num0;
keyMap[hcap.key.Code.NUM_1] = KeyCodes.Num1;
keyMap[hcap.key.Code.NUM_2] = KeyCodes.Num2;
keyMap[hcap.key.Code.NUM_3] = KeyCodes.Num3;
keyMap[hcap.key.Code.NUM_4] = KeyCodes.Num4;
keyMap[hcap.key.Code.NUM_5] = KeyCodes.Num5;
keyMap[hcap.key.Code.NUM_6] = KeyCodes.Num6;
keyMap[hcap.key.Code.NUM_7] = KeyCodes.Num7;
keyMap[hcap.key.Code.NUM_8] = KeyCodes.Num8;
keyMap[hcap.key.Code.NUM_9] = KeyCodes.Num9;
keyMap[hcap.key.Code.PORTAL] = KeyCodes.Menu;   // Portal Key is mapped to MENU on pillow speaker
keyMap[hcap.key.Code.CH_DOWN] = KeyCodes.ChDown;
keyMap[hcap.key.Code.CH_UP] = KeyCodes.ChUp;
keyMap[hcap.key.Code.REWIND] = KeyCodes.Rewind;
keyMap[hcap.key.Code.PLAY] = KeyCodes.Play;
keyMap[hcap.key.Code.GUIDE] = KeyCodes.Play;    // Guide is mapped as Play/pause
keyMap[hcap.key.Code.FAST_FORWARD] = KeyCodes.Forward;
keyMap[hcap.key.Code.STOP] = KeyCodes.Stop;
keyMap[hcap.key.Code.MUTE] = KeyCodes.Mute;
keyMap[hcap.key.Code.VOL_DOWN] = KeyCodes.VolumeDown;
keyMap[hcap.key.Code.VOL_UP] = KeyCodes.VolumeUp;

keyMap[hcap.key.Code.RED] = KeyCodes.Red;
keyMap[hcap.key.Code.GREEN] = KeyCodes.Green;
keyMap[hcap.key.Code.YELLOW] = KeyCodes.Yellow;
keyMap[hcap.key.Code.BLUE] = KeyCodes.Blue;

keyMap[461] = KeyCodes.Back;
keyMap[457] = KeyCodes.Stop;
keyMap[403] = KeyCodes.Red;
keyMap[404] = KeyCodes.Green;
keyMap[405] = KeyCodes.Yellow;
keyMap[458] = KeyCodes.Play;
keyMap[602] = KeyCodes.Menu;
keyMap[415] = KeyCodes.Play;
keyMap[413] = KeyCodes.Stop;
keyMap[19] = KeyCodes.Pause;
keyMap[417] = KeyCodes.Forward;
keyMap[412] = KeyCodes.Rewind;
keyMap[427] = KeyCodes.ChUp;
keyMap[428] = KeyCodes.ChDown;
