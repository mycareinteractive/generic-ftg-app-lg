import KeyCodes from './keycodes';

export default class Desktop {
    constructor() {
        this.jwUrl = 'https://content.jwplatform.com/libraries/72SvWyVc.js';
        this.input = 'TV';
    }

    bootstrap() {
        var self = this;
        document.addEventListener('keydown', this._keyHandler.bind(this), false);
    }

    getPlatform() {
        return "DESKTOP";
    }

    getNetworkDevices() {
        return [{
            networkMode: 1,
            name: 'fake-eth0',
            ip: '127.0.0.1',
            mac: '0021F8034F72',
            gateway: '',
            netmask: ''
        }];
    }

    reload() {
        window.location.hash = '';
        window.location.reload();
    }

    reboot() {
        window.location.hash = '';
        window.location.reload();
    }

    playChannel(param) {
        var defer = jQuery.Deferred();

        return defer.promise();
    }

    stopChannel() {
        var defer = jQuery.Deferred();

        return defer.promise();
    }

    // private methods

    _keyHandler(e) {
        console.log('DESKTOP keydown, code ' + e.which);
        var key = keyMap[e.which];
        if (key) {
            App.triggerKey(e, key);
            console.log('Desktop browser key "' + key + '" handled: ' + e.handled);
        }
        else {
            console.log('Desktop browser unrecognized key ' + e.which);
        }
    }
}

const keyMap = {
    8: KeyCodes.Back,
    13: KeyCodes.Enter,
    19: KeyCodes.Pause,         // Pause/Break
    27: KeyCodes.Escape,        // Esc
    33: KeyCodes.PageUp,
    34: KeyCodes.PageDown,
    37: KeyCodes.Left,
    38: KeyCodes.Up,
    39: KeyCodes.Right,
    40: KeyCodes.Down,
    48: KeyCodes.Num0,
    49: KeyCodes.Num1,
    50: KeyCodes.Num2,
    51: KeyCodes.Num3,
    52: KeyCodes.Num4,
    53: KeyCodes.Num5,
    54: KeyCodes.Num6,
    55: KeyCodes.Num7,
    56: KeyCodes.Num8,
    57: KeyCodes.Num9,
    112: KeyCodes.Menu,         // F1
    113: KeyCodes.Exit,         // F2
    118: KeyCodes.ChDown,       // F7
    119: KeyCodes.ChUp,         // F8
    120: KeyCodes.Rewind,       // F9
    121: KeyCodes.Play,         // F10
    122: KeyCodes.Forward,      // F11
    123: KeyCodes.Stop,         // F12
    173: KeyCodes.Mute,         // mute on laptop FN keyboard
    174: KeyCodes.VolumeDown,   // vol- on laptop FN keyboard
    175: KeyCodes.VolumeUp,     // vol+ on laptop FN keyboard
    216: KeyCodes.Menu
};
