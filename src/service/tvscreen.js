import '../css/tv.css';

function TvScreen() {
}

TvScreen.prototype = {

    className: 'tv',

    channels: [],
    channelIndex: -1,
    channelKeyed: null,
    currentChannel: {},

    initialize: function (options) {
    },

    playStartChannel: function () {
        console.log('Start channel index: ' + this.channelIndex);
        if (this.channelIndex > 0) {
            this.playTv(this.channelIndex);
        }
        else {
            this.playTv(0);
        }
    },

    stopTv: function () {
        console.log('Stopping channel...');
        $('body').removeClass('tv');
        App.device.stopChannel();
    },

    playTv: function (idx) {
        if (!this.channels) {
            console.log('Channel list is empty, nothing to play.');
            return;
        }

        if (!this.channels[idx]) {
            console.log('Channel index ' + idx + ' out of bound.');
            return;
        }

        this.currentChannel = this.channels[idx];
        App.data.lastChannel = this.currentChannel.number;

        var ch = this.currentChannel;
        var num = parseInt(this.currentChannel.number);

        console.log('We have officially changed the channel!');
        this.showLabel(num + ' - ' + this.currentChannel.name, 5000, true);
        //$('#channel-label').hide();

        console.log('Changing channel to ' + num + ' ...');
        App.device.playChannel({
            "mode": ch.tuningMode,
            "type": ch.type,
            "logicalNumber": num,
            "majorNumber": parseInt(ch.param1),
            "minorNumber": parseInt(ch.param2),
            "ip": ch.param1,
            "port": parseInt(ch.param2),
            "onSuccess": function () {
                console.log("channel change done.");
            },
            "onFailure": function (f) {
                console.log("onFailure : errorMessage = " + f.errorMessage);
            }
        });

        console.log('We are attempting to play channel: ' + JSON.stringify(this.currentChannel));
    },

    playTvByNumber: function (num) {
        console.log('Attempting to play channel number = ' + num);
        for (var i = 0; i < this.channels.length; i++) {
            if (this.channels[i].number == num) {
                this.currentChannel = this.channels[i];
                this.channelIndex = i;
                console.log('We are going to play the TV idx ' + i);
                return this.playTv(i);
            }
        }
    },

    showLabel: function (text, ms, complete) {
        ms = ms || 5000;
        $('#channel-label').removeClass('complete').show().find('#loadinginfo').text(text);
        if(complete) {
            $('#channel-label').addClass('complete');
        }
        $.doTimeout('tv label');
        $.doTimeout('tv label', ms, function () {
            $('#channel-label').hide();
            return false;
        });
    },

    keyChannel: function (key) {
        var self = this;
        var cLength = 1;
        $.doTimeout('keycheck');

        var pKey = self.channelKeyed;

        if (pKey) {
            self.channelKeyed = '';
            pKey = pKey + key;

            cLength = pKey.length;
            if (cLength == 3) {
                self.keyChannelDone(pKey);
                return pKey;
            }
            key = pKey;
        }

        if (cLength == 1 && key == '0')
            return;

        this.showLabel(key, 3000);

        self.channelKeyed = key;
        $.doTimeout('keycheck', 3000, function () {
            self.keyChannelDone(self.channelKeyed);
            return false;
        }.bind(this));
        return key;
    },

    keyChannelDone: function (key) {
        console.log('in keyChannelDone ' + key);
        $.doTimeout('keycheck');
        this.showLabel(key, 500);
        this.channelKeyed = '';
        this.playTvByNumber(key);
        return;
    },

    onScreenShow: function () {
        console.log('onScreenShow: Showing the TV screen');
        $('body').addClass('tv');
    },

    onScreenHide: function () {
        console.log('onScreenHide: Hiding the TV screen');
        $('body').removeClass('tv');
        this.stopTv();
    },

    onKey: function (e, key) {
        var currentChannel = this.currentChannel;

        switch (key) {
            case 'ENTER':
                this.keyChannelDone(this.channelKeyed);
                break;
            case 'MENU':
                window.App.checkServer();
                break;
            case 'BACK':
            case 'EXIT':
                break;
            case 'UP':
            case 'CHUP':
                this.channelIndex++;
                if (this.channelIndex >= this.channels.length)
                    this.channelIndex = 0;
                this.playTv(this.channelIndex);
                break;
            case 'DOWN':
            case 'CHDN':
                this.channelIndex--;
                if (this.channelIndex < 0)
                    this.channelIndex = this.channels.length - 1;
                this.playTv(this.channelIndex);
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                this.keyChannel(key);
                break;
        }

        return true;
    }
};

export default TvScreen;